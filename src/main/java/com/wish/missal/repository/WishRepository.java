package com.wish.missal.repository;

import com.wish.missal.domain.Wish;
import org.springframework.data.jpa.repository.JpaRepository;


public interface WishRepository extends JpaRepository<Wish, Long> {

    Wish getWishByEmail(String email);

}
