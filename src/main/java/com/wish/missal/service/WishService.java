package com.wish.missal.service;

import com.wish.missal.domain.Wish;

import java.util.List;

public interface WishService {

    Wish save(Wish wish);

    Wish loadWish(String email);

    List<Wish> loadWishes();
}
