package com.wish.missal.service.impl;

import com.wish.missal.domain.Wish;
import com.wish.missal.repository.WishRepository;
import com.wish.missal.service.WishService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author ikozompolis
 * created on 6/30/2021
 */
@Data
@Service
public class WishServiceImpl implements WishService {

    @Autowired
    private WishRepository wishRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public Wish save(Wish wish) {
        return wishRepository.save(wish);
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    @Override
    public Wish loadWish(String email) {
        return wishRepository.getWishByEmail(email);
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    @Override
    public List<Wish> loadWishes() {
        return wishRepository.findAll();
    }
}
