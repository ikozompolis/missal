package com.wish.missal.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author ikozompolis
 * created on 6/29/2021
 */

@Table(name = "wish")
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class Wish implements Serializable{

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wish_id_seq")
    @SequenceGenerator(name = "wish_id_seq", sequenceName = "wish_id_seq", allocationSize = 1, initialValue = 1)
    private Long id;
    @Column
    private String name;
    @Column
    private String email;
    @Column
    private String picture;
    @Column(name = "wish_text")
    private String wishText;
    @Column
    private boolean approved;
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

}
