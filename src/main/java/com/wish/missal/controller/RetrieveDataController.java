package com.wish.missal.controller;

import com.wish.missal.domain.Wish;
import com.wish.missal.service.WishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ikozompolis
 * created on 6/30/2021
 */
@RestController
public class RetrieveDataController {

    @Autowired
    private WishService wishService;

    @GetMapping("/loadWishes")
    public ResponseEntity<List<Wish>> loadWishes(){
        List<Wish> wishes = new ArrayList<>();
        wishes = wishService.loadWishes();
        return new ResponseEntity<List<Wish>>(wishes, HttpStatus.OK);
    }
}
