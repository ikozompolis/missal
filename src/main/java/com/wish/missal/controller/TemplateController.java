package com.wish.missal.controller;

import com.wish.missal.domain.Wish;
import com.wish.missal.service.WishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ikozompolis
 * created on 6/29/2021
 */
@Controller
public class TemplateController {

    private static final String authorizationRequestBaseUri = "oauth2/authorize-client";
    Map<String, String> oauth2AuthenticationUrls = new HashMap<>();

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;
    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;
    @Autowired
    private WishService wishService;

    @GetMapping("/oauth_login")
    public String getLoginPage(Model model) {
        Iterable<ClientRegistration> clientRegistrations = null;
        ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository)
                .as(Iterable.class);
        if (type != ResolvableType.NONE && ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
            clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
        }

        clientRegistrations.forEach(registration -> oauth2AuthenticationUrls.put(registration.getClientName(), authorizationRequestBaseUri + "/" + registration.getRegistrationId()));
        model.addAttribute("urls", oauth2AuthenticationUrls);

        return "oauth_login";
    }

    @GetMapping("/submitWish")
    public String getSubmitWish(Model model, OAuth2AuthenticationToken authentication) {
        Wish wishToSave = loadUserInfo(new Wish(), authentication);
        if (wishToSave.getEmail() == null){
            model.addAttribute("errorMessage", "Δυστυχώς δεν μπορούμε να βρούμε το Email σας παρακαλώ προσπαθήστε ξανά!");
            return "error";
        }

        wishToSave = wishService.loadWish(wishToSave.getEmail());
        if (wishToSave != null) {
            model.addAttribute(wishToSave);
            return "changeMyWish";
        }
        Wish wish = new Wish();
        model.addAttribute(wish);
        return "submitWish";
    }

    @PostMapping("/submitWish")
    private String submitWish(@ModelAttribute Wish wish, Model model, OAuth2AuthenticationToken authentication){

        Wish wishToSave = loadUserInfo(wish, authentication);

        if (wishToSave.getEmail() != null){
            wishToSave.setApproved(false);
            wishToSave.setCreationDate(LocalDateTime.now());
            wishToSave = wishService.save(wishToSave);
            model.addAttribute(wishToSave);
            return "resultPage";
        }else {
            model.addAttribute("errorMessage", "Δυστυχώς δεν μπορούμε να βρούμε το Email σας παρακαλώ προσπαθήστε ξανά!");
            return "error";
        }
    }

    @PostMapping("/changeMyWish")
    private String changeMyWish(@ModelAttribute Wish wish, Model model, OAuth2AuthenticationToken authentication){

        Wish wishToSave = loadUserInfo(wish, authentication);

        if (wishToSave.getEmail() != null){
            Wish oldWish = wishService.loadWish(wishToSave.getEmail());
            if (oldWish != null) {
                wishToSave.setId(oldWish.getId());
                wishToSave.setApproved(false);
                wishToSave.setCreationDate(LocalDateTime.now());
                wishToSave = wishService.save(wishToSave);
                model.addAttribute(wishToSave);
                return "resultPage";
            }
        }

        model.addAttribute("errorMessage", "Δυστυχώς δεν μπορούμε να βρούμε το Email σας παρακαλώ προσπαθήστε ξανά!");
        return "error";
    }

    private Wish loadUserInfo(Wish wish, OAuth2AuthenticationToken authentication){

        OAuth2AuthorizedClient client = authorizedClientService.loadAuthorizedClient(authentication.getAuthorizedClientRegistrationId(), authentication.getName());

        String userInfoEndpointUri = client.getClientRegistration()
                .getProviderDetails()
                .getUserInfoEndpoint()
                .getUri();

        if (!StringUtils.isEmpty(userInfoEndpointUri)) {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken()
                    .getTokenValue());

            HttpEntity<String> entity = new HttpEntity<String>("", headers);

            ResponseEntity<Map> response = restTemplate.exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);
            Map userAttributes = response.getBody();
            wish.setName((String) userAttributes.get("name"));
            wish.setEmail((String) userAttributes.get("email"));
            wish.setPicture((String) userAttributes.get("picture"));
        }

        return wish;
    }

}
