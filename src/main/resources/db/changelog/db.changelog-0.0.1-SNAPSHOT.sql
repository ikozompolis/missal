--liquibase formatted sql

----------------------------------------------------------------------------------------------------------

--changeset Ioannis.Kozompolis:0.0.1-SNAPSHOT.1

----------------------------------------------------------------------------------------------------------

create table wish (
	id BIGSERIAL not null,
	name VARCHAR(20) not null,
	surname VARCHAR(20) not null,
	wish_text VARCHAR(2000) not null,
	approved BOOLEAN not null default false,
	creation_date TIMESTAMP not null
);

----------------------------------------------------------------------------------------------------------

--changeset Ioannis.Kozompolis:0.0.1-SNAPSHOT.2

alter table wish alter column name TYPE VARCHAR(50);
alter table wish drop column surname;
alter table wish add column picture VARCHAR(2000);
alter table wish add column email VARCHAR(100);

-----------------------------------------------------------------------------------------------------------